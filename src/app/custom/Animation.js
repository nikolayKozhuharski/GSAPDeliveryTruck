import gsap from "gsap/all";
import selectors from "./selectors";

export default class Animation {
  constructor() {
    this._playBtn = document.querySelector("#play-button");
    this._tl = gsap.timeline();
    this._tl.pause();
    this.init();
  }

  async init() {
    const playBtn = selectors.playBtn;
    const truckBtn = selectors.truckBtn;
    const pauseBtn = selectors.pauseBtn;
    const reverseBtn = selectors.reverseBtn;
    const list1 = selectors.list;
    const list2 = selectors.list;
    const listItems = selectors.listItems;
    const containerParts = selectors.container;
    const container = selectors.container;
    const truckBtnBg = selectors.truckBtnBg;
    const backWheel = selectors.backWheel1;
    const backWhee2 = selectors.backWheel2;
    const backWheelBack1 = selectors.backWheelBack1;
    const backWheelBack2 = selectors.backWheelBack2;
    const frontWheel1 = selectors.frontWheel1;
    const frontWheel2 = selectors.frontWheel2;
    const frontWheelsBack = selectors.frontWheelsBack;
    const frontGroup = selectors.frontGroup;
    const truck = selectors.truck;
    const shippedLabel = selectors.shippedLabel;

    this._tl
      .to(list1, { id: "listUp", y: -100 })
      .to(list2, { id: "listDown", y: "+=100", duration: 0.2 })
      .to(listItems, { id: "listItem0", y: "+=100", opacity: 0 }, "-=0.1")
      .to(truckBtnBg, { id: "truckBtnScaleUp", scale: 1.1, duration: 0.2, transformOrigin: "center" })
      .to(truckBtnBg, { id: "truckBtnScaleDown", scale: 1 })
      .to(container, { id: "container", opacity: 1 })
      .to(containerParts, { id: "containerParts", opacity: 1 })
      .to(backWheel, { id: "backWheel1", opacity: 1 }, "backWheel")
      .to(backWhee2, { id: "backWheel2", opacity: 1 }, "backWheel")
      .to(backWheelBack1, { id: "backWheelBack1", opacity: 1 }, "backWheel")
      .to(backWheelBack2, { id: "backWheelBack2", opacity: 1 }, "backWheel")
      .to(frontGroup, { id: "frontGroup", opacity: 1 }, "frontWheel")
      .to(frontWheel1, { id: "frontWheel1", opacity: 1 }, "frontWheel")
      .to(frontWheel2, { id: "frontWheel2", opacity: 1 }, "frontWheel")
      .to(frontWheelsBack, { id: "frontWheelsBack", opacity: 1 }, "frontWheel")
      .to(truck, { id: "truckMovement", x: "-=100" })
      .to(truck, { id: "listItem1", x: "+=500", opacity:0 })
      .to(shippedLabel, { id: "shippedLabel", opacity: 2 }, "shippedLabel")
      .to(playBtn, { id: "listItem2", opacity: 0}, "shippedLabel")
      .to(pauseBtn, { id: "pauseBtn", opacity: 0 }, "shippedLabel")
      .to(reverseBtn, { id: "reverseBtn", opacity: 0 }, "shippedLabel")



      
      playBtn.addEventListener("click", () => {
        if (this._tl.paused()) {
        this._tl.play();
      } else {
        this._tl.restart();
        this._tl.play();
      }
    });
    truckBtn.addEventListener("click", () => {
      this._tl.restart();
      this._tl.play();
    });

    pauseBtn.addEventListener("click", () => {
      this._tl.pause();
    });
    reverseBtn.addEventListener("click", () => {
      this._tl.reverse()
    });
  }
}
